use crate::entity::Entity;
use crate::point::Point;

pub struct Neighbourhood<'a> {
    pub close: Vec<&'a Entity>,
    pub too_close: Vec<&'a Entity>,
}

impl<'a> Neighbourhood<'a> {
    pub fn detect_neighbours(entity: &'a Entity, entities: &'a [Entity]) -> Neighbourhood<'a> {
        let rough_guess = Self::rough_guess(entity, entities);
        let close = Self::find_close(entity, rough_guess.clone());
        let too_close = Self::find_too_close(entity, rough_guess.clone());

        Neighbourhood { close: close.to_vec(), too_close: too_close.to_vec() }
    }

    pub fn centre_of_what_is_close(&self) -> Point {
        let point_sum: Point = self
            .close
            .iter()
            .map(|entity| entity.position.clone())
            .sum();
        point_sum / (self.close.len() as f64)
    }

    pub fn velocity_of_what_is_close(&self) -> Point {
        let point_sum: Point = self
            .close
            .iter()
            .map(|entity| entity.velocity.to_point())
            .sum();
        point_sum / (self.close.len() as f64)
    }

    pub fn centre_of_what_is_too_close(&self) -> Point {
        let point_sum: Point = self
            .too_close
            .iter()
            .map(|entity| entity.position.clone())
            .sum();
        point_sum / (self.too_close.len() as f64)
    }

    pub fn nothing_close(&self) -> bool {
        self.close.is_empty()
    }

    pub fn nothing_too_close(&self) -> bool {
        self.too_close.is_empty()
    }

    fn rough_guess(entity: &'a Entity, others: &'a [Entity]) -> Vec<&'a Entity> {
        others
            .iter()
            .filter(|other| entity.id != other.id)
            .filter(|other| (entity.position.x - entity.close) <= other.position.x)
            .filter(|other| other.position.x <= (entity.position.x + entity.close))
            .filter(|other| (entity.position.y - entity.close) <= other.position.y)
            .filter(|other| other.position.y <= (entity.position.y + entity.close))
            .collect()
    }

    fn find_close(entity: &'a Entity, others: Vec<&'a Entity>) -> Vec<&'a Entity> {
        others
            .into_iter()
            .filter(|other| entity.position.distance(&other.position) <= entity.close)
            .collect()
    }

    fn find_too_close(entity: &'a Entity, others: Vec<&'a Entity>) -> Vec<&'a Entity> {
        others
            .into_iter()
            .filter(|other| entity.position.distance(&other.position) <= entity.too_close)
            .collect()
    }
}
