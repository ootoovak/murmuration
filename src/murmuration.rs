use crate::entity::Entity;
use crate::neighbourhood::Neighbourhood;
use crate::vector::Vector;

mod behaviour;

pub struct Murmuration {
    entities: Vec<Entity>,
    behaviours: Vec<fn(&Entity, &Neighbourhood) -> Vector>,
}

impl Murmuration {
    pub fn empty() -> Self {
        Self {
            entities: Vec::new(),
            behaviours: Vec::new(),
        }
    }

    pub fn new(entities: Vec<Entity>) -> Self {
        Self {
            entities,
            behaviours: vec![
                behaviour::cohesion,
                behaviour::alignment,
                behaviour::separation,
            ],
        }
    }

    pub fn entities(&self) -> &Vec<Entity> {
        &self.entities
    }

    pub fn step(&mut self) {
        self.entities = self
            .entities
            .iter()
            .map(|entity| self.step_entity(entity))
            .collect();
    }

    fn step_entity(&self, entity: &Entity) -> Entity {
        let neighbourhood = Neighbourhood::detect_neighbours(&entity, &self.entities);
        let velocity_delta = self.behaviour_reaction(entity, &neighbourhood);
        entity.r#move(&velocity_delta)
    }

    fn behaviour_reaction(&self, entity: &Entity, neighbourhood: &Neighbourhood) -> Vector {
        self.behaviours
            .iter()
            .map(|behaviour| behaviour(&entity, &neighbourhood))
            .sum()
    }
}
