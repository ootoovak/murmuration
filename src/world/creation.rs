use crate::murmuration::Murmuration;
use crate::world::World;
use quicksilver::{combinators::result, graphics::Font, lifecycle::Asset, Future};

const FONT_FILE_PATH: &str = "Anonymous_Pro/AnonymousPro-Bold.ttf";

pub fn build() -> World {
    let font = Asset::new(Font::load(FONT_FILE_PATH).and_then(|font| result(Ok(font))));
    let murmuration = Murmuration::empty();

    World::new(font, murmuration)
}
