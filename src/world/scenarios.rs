use crate::entity::Entity;
use crate::murmuration::Murmuration;
use crate::world::World;

const SCENARIO_SIZE: f64 = 5.0;
const SCENARIO_INFLUENCE: f64 = 50.0;
const SCENARIO_AVOID: f64 = 20.0;

pub fn load_random(world: &mut World) {
    let entity_group = Entity::with_attributes(SCENARIO_SIZE, SCENARIO_INFLUENCE, SCENARIO_AVOID);
    let entities = (0..2000)
        .map(|_number| entity_group.with_random_attributes())
        .collect();

    world.murmuration = Murmuration::new(entities);
}

pub fn load_one(world: &mut World) {
    let entity1 = Entity::with_attributes(SCENARIO_SIZE, SCENARIO_INFLUENCE, SCENARIO_AVOID)
        .at_position(960.0, 540.0)
        .with_velocity(0.5, 270.0);
    let entity2 = Entity::with_attributes(SCENARIO_SIZE, SCENARIO_INFLUENCE, SCENARIO_AVOID)
        .at_position(860.0, 440.0)
        .with_velocity(0.5, 90.0);
    let entities = vec![entity1, entity2];
    world.murmuration = Murmuration::new(entities);
}

pub fn load_two(world: &mut World) {
    let entity1 = Entity::with_attributes(SCENARIO_SIZE, SCENARIO_INFLUENCE, SCENARIO_AVOID)
        .at_position(960.0, 540.0)
        .with_velocity(0.5, 270.0);
    let entity2 = Entity::with_attributes(SCENARIO_SIZE, SCENARIO_INFLUENCE, SCENARIO_AVOID)
        .at_position(930.0, 440.0)
        .with_velocity(0.5, 90.0);
    let entities = vec![entity1, entity2];
    world.murmuration = Murmuration::new(entities);
}

pub fn load_three(world: &mut World) {
    let entity1 = Entity::with_attributes(SCENARIO_SIZE, SCENARIO_INFLUENCE, SCENARIO_AVOID)
        .at_position(960.0, 540.0)
        .with_velocity(0.0, 0.0);
    let entity2 = Entity::with_attributes(SCENARIO_SIZE, SCENARIO_INFLUENCE, SCENARIO_AVOID)
        .at_position(930.0, 440.0)
        .with_velocity(0.5, 90.0);
    let entity3 = Entity::with_attributes(SCENARIO_SIZE, SCENARIO_INFLUENCE, SCENARIO_AVOID)
        .at_position(990.0, 440.0)
        .with_velocity(0.5, 90.0);
    let entities = vec![entity1, entity2, entity3];
    world.murmuration = Murmuration::new(entities);
}

pub fn load_four(world: &mut World) {
    let entity1 = Entity::with_attributes(SCENARIO_SIZE, SCENARIO_INFLUENCE, SCENARIO_AVOID)
        .at_position(960.0, 540.0)
        .with_velocity(0.0, 0.0);
    let entity2 = Entity::with_attributes(SCENARIO_SIZE, SCENARIO_INFLUENCE, SCENARIO_AVOID)
        .at_position(920.0, 440.0)
        .with_velocity(1.0, 90.0);
    let entity3 = Entity::with_attributes(SCENARIO_SIZE, SCENARIO_INFLUENCE, SCENARIO_AVOID)
        .at_position(920.0, 640.0)
        .with_velocity(1.0, -90.0);
    let entities = vec![entity1, entity2, entity3];
    world.murmuration = Murmuration::new(entities);
}

pub fn load_five(world: &mut World) {
    let entity1 = Entity::with_attributes(SCENARIO_SIZE, SCENARIO_INFLUENCE, SCENARIO_AVOID)
        .at_position(960.0, 540.0)
        .with_velocity(0.5, 270.0);
    let entity2 = Entity::with_attributes(SCENARIO_SIZE, SCENARIO_INFLUENCE, SCENARIO_AVOID)
        .at_position(920.0, 440.0)
        .with_velocity(0.5, 90.0);
    let entities = vec![entity1, entity2];
    world.murmuration = Murmuration::new(entities);
}

pub fn load_six(world: &mut World) {
    let entity1 = Entity::with_attributes(SCENARIO_SIZE, SCENARIO_INFLUENCE, SCENARIO_AVOID)
        .at_position(960.0, 540.0)
        .with_velocity(0.5, 270.0);
    let entity2 = Entity::with_attributes(SCENARIO_SIZE, SCENARIO_INFLUENCE, SCENARIO_AVOID)
        .at_position(940.0, 440.0)
        .with_velocity(0.5, 90.0);
    let entities = vec![entity1, entity2];
    world.murmuration = Murmuration::new(entities);
}

pub fn load_seven(world: &mut World) {
    let entity1 = Entity::with_attributes(SCENARIO_SIZE, SCENARIO_INFLUENCE, SCENARIO_AVOID)
        .at_position(960.0, 540.0)
        .with_velocity(0.0, 0.0);
    let entity2 = Entity::with_attributes(SCENARIO_SIZE, SCENARIO_INFLUENCE, SCENARIO_AVOID)
        .at_position(940.0, 440.0)
        .with_velocity(0.5, 90.0);
    let entity3 = Entity::with_attributes(SCENARIO_SIZE, SCENARIO_INFLUENCE, SCENARIO_AVOID)
        .at_position(980.0, 440.0)
        .with_velocity(0.5, 90.0);
    let entities = vec![entity1, entity2, entity3];
    world.murmuration = Murmuration::new(entities);
}

pub fn load_eight(world: &mut World) {
    let entity1 = Entity::with_attributes(SCENARIO_SIZE, SCENARIO_INFLUENCE, SCENARIO_AVOID)
        .at_position(960.0, 540.0)
        .with_velocity(0.0, 0.0);
    let entity2 = Entity::with_attributes(SCENARIO_SIZE, SCENARIO_INFLUENCE, SCENARIO_AVOID)
        .at_position(940.0, 440.0)
        .with_velocity(1.0, 90.0);
    let entity3 = Entity::with_attributes(SCENARIO_SIZE, SCENARIO_INFLUENCE, SCENARIO_AVOID)
        .at_position(940.0, 640.0)
        .with_velocity(1.0, -90.0);
    let entities = vec![entity1, entity2, entity3];
    world.murmuration = Murmuration::new(entities);
}
