use crate::entity::Entity;
use crate::world::World;

use quicksilver::{
    geom::{Circle, Line, Transform},
    graphics::{Background::Col, Background::Img, Color, FontStyle},
    lifecycle::Window,
};

const DRAW_SCALE: f64 = 75.0;

fn clear_screen(window: &mut Window) {
    window
        .clear(Color::BLACK)
        .expect("Failed to clear the current window.");
}

fn draw_entities(entities: &[Entity], window: &mut Window, debugging_visualisation: bool) {
    entities
        .iter()
        .for_each(|entity| draw_entity(entity, window, debugging_visualisation));
}

fn draw_entity(entity: &Entity, window: &mut Window, debugging_visualisation: bool) {
    let px = entity.position.x as i32;
    let py = entity.position.y as i32;
    let vp = entity.velocity.to_point();
    let vx = px + (DRAW_SCALE * vp.x) as i32;
    let vy = py + (DRAW_SCALE * vp.y) as i32;

    if debugging_visualisation {
        window.draw_ex(
            &Circle::new((px, py), entity.close as f32),
            Col(Color::WHITE.with_alpha(0.1)),
            Transform::IDENTITY,
            1,
        );

        window.draw_ex(
            &Circle::new((px, py), entity.too_close as f32),
            Col(Color::WHITE.with_alpha(0.1)),
            Transform::IDENTITY,
            2,
        );

        window.draw_ex(
            &Line::new((px, py), (vx, vy)).with_thickness(2.0),
            Col(Color::GREEN.with_alpha(0.25)),
            Transform::IDENTITY,
            4,
        );
    }

    window.draw_ex(
        &Circle::new((px, py), entity.size as f32),
        Col(Color::YELLOW.with_alpha(1.0)),
        Transform::IDENTITY,
        3,
    );
}

fn draw_system_information(world: &mut World, window: &mut Window) {
    let fps = format!(
        "C-FPS: {}\nA-FPS: {}",
        window.current_fps(),
        window.average_fps()
    );

    let style = FontStyle::new(24.0, Color::WHITE);

    world
        .font
        .execute(|font| {
            let text = font.render(&fps, &style).unwrap();
            window.draw(&text.area(), Img(&text));
            Ok(())
        })
        .unwrap();
}

pub fn draw(world: &mut World, window: &mut Window) {
    clear_screen(window);
    draw_entities(world.entities(), window, world.debugging_visualisation);
    draw_system_information(world, window);
}
