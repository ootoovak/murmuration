use crate::world::{scenarios, World};

use quicksilver::{
    input::{
        ButtonState::Pressed,
        Key::{Escape, Key1, Key2, Key3, Key4, Key5, Key6, Key7, Key8, Return, Space, D, R},
    },
    lifecycle::{Event, Event::Key, Window},
};

pub fn handler(world: &mut World, event: &Event, window: &mut Window) {
    match event {
        Key(D, Pressed) => world.toggle_debugging_visualisation(),
        Key(Escape, Pressed) => window.close(),
        Key(Key1, Pressed) => scenarios::load_one(world),
        Key(Key2, Pressed) => scenarios::load_two(world),
        Key(Key3, Pressed) => scenarios::load_three(world),
        Key(Key4, Pressed) => scenarios::load_four(world),
        Key(Key5, Pressed) => scenarios::load_five(world),
        Key(Key6, Pressed) => scenarios::load_six(world),
        Key(Key7, Pressed) => scenarios::load_seven(world),
        Key(Key8, Pressed) => scenarios::load_eight(world),
        Key(R, Pressed) => scenarios::load_random(world),
        Key(Return, Pressed) => world.step(),
        Key(Space, Pressed) => world.toggle_pause(),
        _ => {}
    };
}
