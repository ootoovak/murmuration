use crate::world::World;

pub fn calculate(world: &mut World) {
    if world.running() {
        world.step()
    };
}
