extern crate quicksilver;
extern crate rand;
extern crate uuid;

mod entity;
mod murmuration;
mod neighbourhood;
mod point;
mod vector;
mod world;

use quicksilver::{
    geom,
    lifecycle::{run, Event, Settings, State, Window},
    Result,
};
use world::World;

const WINDOW_HEIGHT: u32 = 1080;
const WINDOW_NAME: &str = "Cohesion Test";
const WINDOW_WIDTH: u32 = 1920;

impl State for World {
    fn new() -> Result<World> {
        let world = world::creation::build();
        Ok(world)
    }

    fn event(&mut self, event: &Event, window: &mut Window) -> Result<()> {
        world::event::handler(self, event, window);
        Ok(())
    }

    fn update(&mut self, _window: &mut Window) -> Result<()> {
        world::update::calculate(self);
        Ok(())
    }

    fn draw(&mut self, window: &mut Window) -> Result<()> {
        world::display::draw(self, window);
        Ok(())
    }
}

fn main() {
    run::<World>(
        WINDOW_NAME,
        geom::Vector::new(WINDOW_WIDTH, WINDOW_HEIGHT),
        Settings {
            fullscreen: true,
            max_updates: 10,
            ..Settings::default()
        },
    );
}
