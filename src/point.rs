use crate::vector::Vector;
use rand::Rng;
use std::f64::consts::PI;
use std::iter::Sum;
use std::ops::Add;
use std::ops::Div;
use std::ops::Sub;

const WINDOW_HEIGHT: u32 = 1080;
const WINDOW_WIDTH: u32 = 1920;

#[derive(Debug, Clone)]
pub struct Point {
    pub x: f64,
    pub y: f64,
}

impl Point {
    pub fn zero() -> Point {
        Self { x: 0.0, y: 0.0 }
    }

    pub fn random() -> Point {
        let mut rng = rand::thread_rng();
        Self {
            x: rng.gen_range(0.0, f64::from(WINDOW_WIDTH)),
            y: rng.gen_range(0.0, f64::from(WINDOW_HEIGHT)),
        }
    }

    pub fn to_vector(&self) -> Vector {
        Vector {
            magnitude: self.magnitude(),
            direction: self.direction(),
        }
    }

    pub fn relative_to_vector(&self, other: &Point) -> Vector {
        let relative_point = other - self;
        Vector {
            magnitude: relative_point.magnitude(),
            direction: relative_point.direction(),
        }
    }

    pub fn distance(&self, other: &Point) -> f64 {
        let relative_point = other - self;
        (relative_point.x.powi(2) + relative_point.y.powi(2)).sqrt()
    }

    fn magnitude(&self) -> f64 {
        (self.x.powi(2) + self.y.powi(2)).sqrt()
    }

    fn direction(&self) -> f64 {
        let angle = self.y.atan2(self.x) * (180.0 / PI);
        if angle < 0.0 {
            360.0 + angle
        } else {
            angle
        }
    }
}

impl Add for &Point {
    type Output = Point;

    fn add(self, other: &Point) -> Point {
        Point {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

impl Sub for &Point {
    type Output = Point;

    fn sub(self, other: &Point) -> Point {
        Point {
            x: self.x - other.x,
            y: self.y - other.y,
        }
    }
}

impl Add for Point {
    type Output = Point;

    fn add(self, other: Point) -> Point {
        Point {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

impl Sub for Point {
    type Output = Point;

    fn sub(self, other: Point) -> Point {
        Point {
            x: self.x - other.x,
            y: self.y - other.y,
        }
    }
}

impl Div<f64> for Point {
    type Output = Point;

    fn div(self, rhs: f64) -> Point {
        Point {
            x: self.x / rhs,
            y: self.y / rhs,
        }
    }
}

impl Sum for Point {
    fn sum<I>(iter: I) -> Point
    where
        I: Iterator<Item = Point>,
    {
        iter.fold(Point::zero(), |a, b| a + b)
    }
}
