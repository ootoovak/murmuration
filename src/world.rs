use crate::entity::Entity;
use crate::murmuration::Murmuration;
use quicksilver::{graphics::Font, lifecycle::Asset};

pub mod creation;
pub mod display;
pub mod event;
pub mod scenarios;
pub mod update;

pub struct World {
    pub font: Asset<Font>,
    murmuration: Murmuration,
    running: bool,
    debugging_visualisation: bool,
}

impl World {
    pub fn new(font: Asset<Font>, murmuration: Murmuration) -> Self {
        Self {
            font,
            murmuration,
            running: false,
            debugging_visualisation: false,
        }
    }

    pub fn entities(&self) -> &Vec<Entity> {
        self.murmuration.entities()
    }

    pub fn running(&self) -> bool {
        self.running
    }

    pub fn step(&mut self) {
        self.murmuration.step();
    }

    pub fn toggle_pause(&mut self) {
        self.running = !self.running;
    }

    pub fn toggle_debugging_visualisation(&mut self) {
        self.debugging_visualisation = !self.debugging_visualisation;
    }
}
