use crate::point::Point;
use crate::vector::Vector;
use uuid::Uuid;

const MAXIMUM_VELOCITY: f64 = 0.5;

#[derive(Clone)]
pub struct Entity {
    pub id: Uuid,
    pub size: f64,
    pub position: Point,
    pub velocity: Vector,
    pub close: f64,
    pub too_close: f64,
}

impl Entity {
    pub fn with_attributes(size: f64, close: f64, too_close: f64) -> Entity {
        Entity {
            size,
            close,
            too_close,
            id: Uuid::new_v4(),
            position: Point::zero(),
            velocity: Vector::zero(),
        }
    }

    pub fn with_random_attributes(&self) -> Entity {
        Entity {
            id: Uuid::new_v4(),
            position: Point::random(),
            velocity: Vector::random(),
            ..*self
        }
    }

    pub fn at_position(mut self, x: f64, y: f64) -> Entity {
        self.position = Point { x, y };
        self
    }

    pub fn with_velocity(mut self, magnitude: f64, direction: f64) -> Entity {
        self.velocity = Vector {
            magnitude,
            direction,
        };
        self
    }

    pub fn r#move(&self, velocity_delta: &Vector) -> Entity {
        Entity {
            position: self.update_position(),
            velocity: self.update_velocity(velocity_delta),
            ..*self
        }
    }

    fn update_position(&self) -> Point {
        &self.position + &self.velocity.to_point()
    }

    fn update_velocity(&self, velocity_delta: &Vector) -> Vector {
        let mut new_velocity = &self.velocity + velocity_delta;
        new_velocity.magnitude = new_velocity.magnitude.min(MAXIMUM_VELOCITY);
        new_velocity
    }
}
