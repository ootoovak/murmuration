use crate::point::Point;
use rand::Rng;
use std::f64::consts::PI;
use std::iter::Sum;
use std::ops::Add;
use std::ops::AddAssign;
use std::ops::Div;
use std::ops::Sub;

#[derive(Debug, Clone)]
pub struct Vector {
    pub magnitude: f64,
    pub direction: f64,
}

impl Vector {
    pub fn zero() -> Vector {
        Self {
            magnitude: 0.0,
            direction: 0.0,
        }
    }

    pub fn random() -> Vector {
        let mut rng = rand::thread_rng();
        Self {
            magnitude: rng.gen_range(0.0, 1.0),
            direction: rng.gen_range(0.0, 360.0),
        }
    }

    pub fn to_point(&self) -> Point {
        Point {
            x: self.x(),
            y: self.y(),
        }
    }

    fn x(&self) -> f64 {
        let rad_direction = self.direction * (PI / 180.0);
        self.magnitude * rad_direction.cos()
    }

    fn y(&self) -> f64 {
        let rad_direction = self.direction * (PI / 180.0);
        self.magnitude * rad_direction.sin()
    }
}

impl Add for &Vector {
    type Output = Vector;

    fn add(self, other: &Vector) -> Vector {
        let result = self.to_point() + other.to_point();
        result.to_vector()
    }
}

impl Sub for &Vector {
    type Output = Vector;

    fn sub(self, other: &Vector) -> Vector {
        let result = self.to_point() - other.to_point();
        result.to_vector()
    }
}

impl Add for Vector {
    type Output = Vector;

    fn add(self, other: Vector) -> Vector {
        let result = self.to_point() + other.to_point();
        result.to_vector()
    }
}

impl Sub for Vector {
    type Output = Vector;

    fn sub(self, other: Vector) -> Vector {
        let result = self.to_point() - other.to_point();
        result.to_vector()
    }
}

impl Div<f64> for Vector {
    type Output = Vector;

    fn div(self, rhs: f64) -> Vector {
        (self.to_point() / rhs).to_vector()
    }
}

impl AddAssign for Vector {
    fn add_assign(&mut self, other: Vector) {
        *self = Vector { ..*self } + other;
    }
}

impl Sum for Vector {
    fn sum<I>(iter: I) -> Vector
    where
        I: Iterator<Item = Vector>,
    {
        iter.fold(Vector::zero(), |a, b| a + b)
    }
}
