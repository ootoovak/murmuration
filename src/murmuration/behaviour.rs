use crate::entity::Entity;
use crate::neighbourhood::Neighbourhood;
use crate::vector::Vector;

const COHESION_CONSTANT: f64 = 0.1;
const SEPARATION_CONSTANT: f64 = -0.1;
const ALIGNMENT_CONSTANT: f64 = 0.1;

pub fn alignment(_entity: &Entity, neighbourhood: &Neighbourhood) -> Vector {
    if neighbourhood.nothing_close() {
        return Vector::zero();
    }

    let entities_centre = neighbourhood.velocity_of_what_is_close();

    let mut vector = entities_centre.to_vector();
    vector.magnitude = ALIGNMENT_CONSTANT;

    vector
}

pub fn cohesion(entity: &Entity, neighbourhood: &Neighbourhood) -> Vector {
    if neighbourhood.nothing_close() {
        return Vector::zero();
    }

    let entities_centre = neighbourhood.centre_of_what_is_close();

    let mut vector_to_centre = entity.position.relative_to_vector(&entities_centre);
    vector_to_centre.magnitude = COHESION_CONSTANT;

    vector_to_centre
}

pub fn separation(entity: &Entity, neighbourhood: &Neighbourhood) -> Vector {
    if neighbourhood.nothing_too_close() {
        return Vector::zero();
    }

    let entities_centre = neighbourhood.centre_of_what_is_too_close();

    let mut vector_to_centre = entity.position.relative_to_vector(&entities_centre);
    vector_to_centre.magnitude = SEPARATION_CONSTANT;

    vector_to_centre
}
