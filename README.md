# Murmuration

A simple simulation of a flock of birds like a murmuration of starlings.

## Running

```bash
cargo run
```